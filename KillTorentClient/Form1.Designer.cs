﻿namespace KillTorentClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ProcessIdTxt = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.HourNum = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.MinuteNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.SecondsNum = new System.Windows.Forms.NumericUpDown();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HourNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondsNum)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Torrent Client Process ID";
            // 
            // ProcessIdTxt
            // 
            this.ProcessIdTxt.Location = new System.Drawing.Point(27, 67);
            this.ProcessIdTxt.Name = "ProcessIdTxt";
            this.ProcessIdTxt.Size = new System.Drawing.Size(543, 26);
            this.ProcessIdTxt.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.SecondsNum);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.MinuteNum);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.HourNum);
            this.groupBox1.Location = new System.Drawing.Point(27, 136);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(543, 171);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Shutdown Time (24-hour time)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Hour";
            // 
            // HourNum
            // 
            this.HourNum.Location = new System.Drawing.Point(19, 72);
            this.HourNum.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.HourNum.Name = "HourNum";
            this.HourNum.Size = new System.Drawing.Size(120, 26);
            this.HourNum.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Minutes";
            // 
            // MinuteNum
            // 
            this.MinuteNum.Location = new System.Drawing.Point(184, 72);
            this.MinuteNum.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.MinuteNum.Name = "MinuteNum";
            this.MinuteNum.Size = new System.Drawing.Size(120, 26);
            this.MinuteNum.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(347, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Seconds";
            // 
            // SecondsNum
            // 
            this.SecondsNum.Location = new System.Drawing.Point(347, 72);
            this.SecondsNum.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.SecondsNum.Name = "SecondsNum";
            this.SecondsNum.Size = new System.Drawing.Size(120, 26);
            this.SecondsNum.TabIndex = 4;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(459, 368);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(110, 43);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 435);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ProcessIdTxt);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Kill Torrent Client";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HourNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinuteNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondsNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ProcessIdTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown HourNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown SecondsNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown MinuteNum;
        private System.Windows.Forms.Button btnStart;
    }
}

