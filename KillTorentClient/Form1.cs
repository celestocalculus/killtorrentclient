﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KillTorentClient
{
    public partial class Form1 : Form
    {
        Process process;
        DateTime ShutdownTime;
        bool Started = false;
        IDisposable Subscription;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (Started)
            {
                Subscription.Dispose();
                EnableOrDisableControls(true);
                Started = false;
                return;
            }

            if (string.IsNullOrWhiteSpace(ProcessIdTxt.Text))
            {
                MessageBox.Show("Please provide process ID");
                ProcessIdTxt.Focus();
                return;
            }

            int procId = 0;
            if(!int.TryParse(ProcessIdTxt.Text, out procId))
            {
                MessageBox.Show("Invalid process ID supplied. Value must be numeric.");
                ProcessIdTxt.Focus();
                return;
            }

            try
            {
                process = Process.GetProcessById(procId);
            }
            catch (Exception)
            {
                MessageBox.Show("No process found with the ID supplied.");
                ProcessIdTxt.Focus();
                return;
            }

            var now = DateTime.Now;
            ShutdownTime = new DateTime(now.Year, now.Month, now.Day, (int)HourNum.Value, (int)MinuteNum.Value, (int)SecondsNum.Value);

            if(now > ShutdownTime)
            {
                MessageBox.Show("Shutdown time cannot be in the past");
                return;
            }

            //Start the checker
            EnableOrDisableControls(false);
            IObservable<long> TaskObserver = Observable.Interval(TimeSpan.FromSeconds(20));
            Subscription = TaskObserver.Subscribe(x => Task.Run(() => CheckAndShutdown()));

            Started = true;
        }

        private void EnableOrDisableControls(bool shouldEnable)
        {
            ProcessIdTxt.Invoke(new Action(() => ProcessIdTxt.Enabled = shouldEnable));
            HourNum.Invoke(new Action(() => HourNum.Enabled = shouldEnable));
            MinuteNum.Invoke(new Action(() => MinuteNum.Enabled = shouldEnable));
            SecondsNum.Invoke(new Action(() => SecondsNum.Enabled = shouldEnable));
            btnStart.Invoke(new Action(() => btnStart.Text = shouldEnable ? "Start" : "Stop"));
        }

        private void CheckAndShutdown()
        {
            if (DateTime.Now >= ShutdownTime && Started)
            {
                try
                {
                    var processName = process.ProcessName;
                    process.CloseMainWindow();
                    process.WaitForExit();

                    EnableOrDisableControls(true);
                    Started = false;

                    MessageBox.Show(string.Format("Stopped process {0} at {1: HH:mm:ss}", processName, ShutdownTime));
                }
                catch (Exception)
                {
                    MessageBox.Show("An exception occured while stopping the process, but don't worry you're safe!");
                }
            }
        }
    }
}
